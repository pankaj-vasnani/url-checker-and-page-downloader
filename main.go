package main

import(
	"fmt"
	"net/http"
	"io/ioutil"
	"strings"
	"log"
	"sync"
	"runtime"
)

func checkAndSavebody(url string, wg *sync.WaitGroup) {
	resp, err := http.Get(url)

	if(err != nil) {
		fmt.Println(err)
		fmt.Printf("%s is down\n", url)
	} else {
		defer resp.Body.Close()
		fmt.Printf("%s status code = %d", url, resp.StatusCode)

		if(resp.StatusCode == 200) {
			bodyData, err := ioutil.ReadAll(resp.Body)
			file := strings.Split(url, "//")[1]
			file += ".txt"

			fmt.Printf("Writing response body to %s\n", file)

			// Writing response of body to file
			err = ioutil.WriteFile(file, bodyData, 0664)

			if(err != nil){
				log.Fatal(err)
			}
		}
	}

	wg.Done()
}

func main() {
	urls := []string{"https://www.google.com", "http://www.facebook.com", "http://www.medium.com", "http://www.hello1.com"}

	var wg sync.WaitGroup

	wg.Add(len(urls))

	for _, val := range urls{
		go checkAndSavebody(val, &wg)
		fmt.Println(strings.Repeat("#", 20))
	}

	fmt.Println("No of goroutines:", runtime.NumGoroutine())

	wg.Wait()
}